"use strict";

//项目文件的各种操作
const fs = require("fs");
const settings = require("../settings");

class FileMan {
    static writeFile(filePath, content, okCallback, errCallback) {
        const path = settings.baseProjectPath + "/" + filePath;

        fs.writeFile(path, content, "utf8", err => {
            if (err) {
                errCallback(err);
                return;
            }

            okCallback();
        });
    }
}

module.exports = FileMan;