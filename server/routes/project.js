"use strict";

const _ = require("lodash");
const express = require("express");
const router = express.Router();
const { User, Project } = require("../db/db");

router.post("/", async(req, res) => {
    const operation = req.body.operation;

    if (operation === "ProjectUsers") {
        const findProject = await Project.findOne({
            ProjectName: req.body.projectName,
        });

        if (!findProject) {
            //console.log(findProject);
            return res.send({
                status: "OK",
                data: [],
            });
        }

        let prjUsers = findProject.Member.map(usr => {
            return {
                userName: usr.name,
            };
        });

        const exceptUserName = req.body.userName;
        prjUsers = _.filter(prjUsers, user => {
            return user.userName !== exceptUserName;
        });

        const allUsers = await User.find();
        let users = [];

        prjUsers.forEach(prjUser => {
            const _index = _.findIndex(allUsers, _user => {
                return _user.username === prjUser.userName;
            });

            if (_index >= 0) {
                users.push(allUsers[_index]);
            }
        });

        users.map(user => {
            return {
                userName: user.username,
                nickName: user.nickName,
                avatar: user.avatar,
                color: user.color,
            };
        });

        res.send({
            status: "OK",
            data: users.map(user => {
                return {
                    userName: user.username,
                    nickName: user.nickName,
                    avatar: user.avatar,
                    color: user.color,
                };
            }),
        });
        return;
    }
});

module.exports = router;
