"use strict";

const express = require("express");
const router = express.Router();
const { Project } = require("../db/db");
router.post("/", async(req, res) => {
    if (req.body.projectName.length === 0 || req.body.projectName.length > 30) {
        return res.send({
            status: "failure",
            message: "项目名称输入不对",
        });
    }

    if (req.body.newDesc.length === 0 || req.body.newDesc.length > 30) {
        return res.send({
            status: "failure",
            message: "项目简介输入不对，不能为零或者超过200个字符",
        });
    }

    const condition = { ProjectName: req.body.projectName };
    Project.findOne(condition, function(err, doc) {
        if (err)
            console.log(err)
        if (doc) {
            if (doc.Owner === req.body.userName) {
                doc.ProjectDesc = req.body.newDesc;
                doc.save();
                res.send({
                    status: "success",
                    doc
                })
            } else {
                res.send({
                    status: "failure",
                    message: "仅允许Owner修改项目简介"
                })
            }
        } else {
            res.send({
                status: "failure",
                message: "项目不存在"
            })
        }
    })

});

module.exports = router;