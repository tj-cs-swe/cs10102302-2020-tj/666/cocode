"use strict";

//项目文件的各种操作
const express = require("express");
const router = express.Router();
const { Project } = require("../db/db");
const path = require("path");
const fs = require("fs");
const settings = require("../settings");
const FileMan = require("./fileMan");
const cm = require("../cocodeManage");

function deleteFolder(path) {
    let files = [];
    if (fs.existsSync(path)) {
        files = fs.readdirSync(path);
        files.forEach(function(file) {
            const curPath = path + "/" + file;
            if (fs.statSync(curPath).isDirectory()) {
                // recurse
                deleteFolder(curPath);
            } else {
                // delete file
                fs.unlinkSync(curPath);
            }
        });
        fs.rmdirSync(path);
    }
}

const fileList = []; //文件列表

function getFileList(projectName, projectPath)
{
    fileList.length = 0;
    id = 1;
    fileList.push({
        name: projectName,
        pid: 0,
        id: 1,
        type: 'dir',
    });
    if(mapDir(projectPath, 1))
    {
        return 1;
    }
    return 0;
}

router.post("/", async(req, res) => {
    const operation = req.body.operation;
    let projectPath;
    let path;
    console.log(req.body);
    const findProject = await Project.findOne({
        ProjectName: req.body.projectName,
    });
    if (!findProject) {
        res.send({
            status: "Project_Not_Exist",
        });
        return;
    }

    projectPath = settings.baseProjectPath + req.body.projectName;

    let exists = fs.existsSync(projectPath);
    if (!exists) {
        res.send({
            status: "ProjectDir_Not_Exist",
        });
        return;
    }

    if (operation === "FileList") {
        //文件列表
        
        if (getFileList(req.body.projectName, projectPath)) {

            //console.log(fileList);

            res.send({ status: "OK", filelist: fileList });
        } else {
            res.send({ status: "ERROR" });
        }
        return;
    }

    if (operation === "GetFile") {
        //查看文件
        path = settings.baseProjectPath + "/" + req.body.path;

        fs.readFile(path, "utf-8", function(err, data) {
            if (err) {
                console.log(err);
                res.send({ status: "ERROR" });
            } else {
                console.log(data);
                res.send({ status: "OK", content: data });
            }
        });
        return;
    }

    if (operation === "DeleteFile") {
        //删除文件
        if(cm.hasSameEditFile(req.body.projectName, req.body.path)){
            console.log('文件被使用')
            res.send({status: "IsUsing"})
            return;
        }
        path = settings.baseProjectPath + "/" + req.body.path;

        fs.unlink(path, err => {
            if (err) {
                res.send({ status: "ERROR" });
                return;
            }
            res.send({status:'OK'});
        });
        
        return;
    }

    if (operation === "WriteFile") {
        //写入文件
        FileMan.writeFile(req.body.path, req.body.content, () => {
            console.log("写入文件成功");
            res.send({ status: "OK" });
        }, () => {
            console.log("写入文件失败");
            res.send({ status: "ERROR" });
        });

        return;
    }
    if (operation === "NewFile") {
        //新建文件
        path = settings.baseProjectPath + "/" + req.body.path;

        fs.exists(path, (exists) => {
            if (exists) {
                console.log('新建文件失败')
                res.send({ status: "AlreadyExist" });
                return;
            }
            fs.writeFile(path, '', "utf8", err => {
                if (err) {
                    console.log('新建文件失败')
                    res.send({ status: "ERROR" });
                    return;
                }
                console.log('新建文件成功')
                res.send({status:'OK'});
            });
        });
        return;
    }
    if (operation === "NewDir") {
        path = settings.baseProjectPath + "/" + req.body.path;
        console.log(path);
        console.log(req.body);
        fs.exists(path, (exists) => {
            if (exists) {
                console.log('新建文件失败')
                res.send({ status: "AlreadyExist" });
                return;
            }
            fs.mkdir(path, err => {
                if (err) {
                    console.log('新建文件夹错误')
                    res.send({ status: "ERROR" });
                    return;
                }
                console.log('新建文件夹成功')
                res.send({status:'OK'});
            });
        });
        return;
    }

    if (operation === "RenameFile") {
        //重命名文件
        if(cm.hasSameEditFile(req.body.projectName, req.body.path)){
            console.log('文件被使用')
            res.send({status: "IsUsing"})
            return;
        }
        path = settings.baseProjectPath + "/" + req.body.path;
        const renamePath = settings.baseProjectPath + "/" + req.body.rename;
        fs.exists(renamePath, (exists) => {
            if (exists) {
                console.log('重命名失败')
                res.send({ status: "AlreadyExist" });
                return;
            }
            fs.rename(path, renamePath, err => {
                if (err) {
                    res.send({ status: "ERROR" });
                    return;
                }
                res.send({status:'OK'});
            });
        });
        return;
    }
    if (operation === "DelDir") {
        path = settings.baseProjectPath + "/" + req.body.path;
        deleteFolder(path);
        res.send({status:'OK'});
        return;
    }
});

//递归函数求文件列表
let id = 1;

function mapDir(dir, pid) {
    try {
        const files = fs.readdirSync(dir);

        files.forEach(filename => {
            //console.log(filename);
            let pathname = path.join(dir, filename);
            const stats = fs.statSync(pathname); // 读取文件信息
            if (stats.isDirectory()) {
                const dirMember = {
                    name: filename,
                    pid: pid,
                    id: ++id,
                    type: "dir",
                };
                fileList.push(dirMember);
                if (!mapDir(pathname, id)) {
                    return false;
                }
            } else if (stats.isFile()) {
                const fileMember = {
                    name: filename,
                    pid: pid,
                    id: ++id,
                    type: path.extname(pathname).replace(".", ""),
                };
                fileList.push(fileMember);
            }
        });
        return true;
    } catch (err) {
        return false;
    }
}

module.exports = router;